require 'torch'
require 'optim'
require 'nn'
require 'CustomStochasticGradient'

-- 10 Fold Validation

function getNeuralNetwork(inputSize, hiddenLayer1Size, hiddenLayer2Size, outputSize, learningRate, numIterations)
	local neuralnet = nn.Sequential()

	neuralnet:add(nn.Linear(inputSize, hiddenLayer1Size))
	neuralnet:add(nn.ReLU())
	neuralnet:add(nn.Dropout(0.1))
	neuralnet:add(nn.Linear(hiddenLayer1Size, hiddenLayer2Size))
	neuralnet:add(nn.ReLU())
	neuralnet:add(nn.Dropout(0.1))
	neuralnet:add(nn.Linear(hiddenLayer2Size, outputSize))

	local criterion = nn.MSECriterion() 

	local trainer = nn.CustomStochasticGradient(neuralnet, criterion)
	trainer.learningRate = learningRate
	trainer.maxIteration = numIterations
	trainer.plot = false

	return neuralnet, criterion, trainer
end	

csv2tensor = require 'csv2tensor'

inputSize = 18
outputSize = 6

hiddenLayer1Size = 18
hiddenLayer2Size = 18
learningRate = 0.1
numIterations = 100
numFolds = 10

x,columnNames_x = csv2tensor.load("datasetX.txt")
y,columnNames_y = csv2tensor.load("datasetY.txt")

avg_err = 0;

for fold = 1, numFolds do
	print("Fold : " .. fold)
	local neuralnet, criterion, trainer = getNeuralNetwork(inputSize, hiddenLayer1Size, hiddenLayer2Size, outputSize, learningRate, numIterations)
	local N = x:size(1)
    local i_start = math.floor((fold - 1) * (N / numFolds) + 1)
    local i_end = math.floor(fold * (N / numFolds))

	train={};
	function train:size() return x:size(1)*0.9 end 

	trainX = torch.cat(x:narrow(1, 1, i_start), x:narrow(1, i_end, N - i_end + 1), 1)
	trainY = torch.cat(y:narrow(1, 1, i_start), y:narrow(1, i_end, N - i_end + 1), 1)

	for i=1,train:size() do
	  train[i] = {trainX[i], trainY[i]}
	end

	test={};
	function test:size() return x:size(1)*0.1 end

    testX = x:narrow(1, i_start, i_end - i_start + 1)
    testY = y:narrow(1, i_start, i_end - i_start + 1)

	for i=1,test:size() do
		test[i] = {testX[i], testY[i]}
	end	

	-- Training
	trainer:train(train)

	neuralnet:evaluate()
	err = 0

	-- Testing
	for iteration = 1, test:size() do
	    local sample = test[ math.random(test:size()) ]
	    local inputs = sample[1]
	    local targets = sample[2]

	    local outputs = neuralnet:forward(inputs)
	    err = err + criterion:forward(outputs, targets)
	end 

	err = err / test:size()
	avg_err = avg_err + err
	print(string.format("Fold %d Testing results: err= %f", fold, err))
end
print("Average error after 10-fold cross validation : " .. (avg_err/numFolds))