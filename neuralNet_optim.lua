require 'torch'
require 'nn'
require 'optim'
require 'CustomStochasticGradient'

csv2tensor = require 'csv2tensor'

neuralnet = nn.Sequential()

inputSize = 18
outputSize = 6

hiddenLayer1Size = 18
hiddenLayer2Size = 18
numIterations = 200
rho = 100

logger = optim.Logger('loss_log.txt')

neuralnet:add(nn.Linear(inputSize, hiddenLayer1Size))
neuralnet:add(nn.ReLU())
neuralnet:add(nn.Dropout(0.1))
neuralnet:add(nn.Linear(hiddenLayer1Size, hiddenLayer2Size))
neuralnet:add(nn.ReLU())
neuralnet:add(nn.Dropout(0.1))
neuralnet:add(nn.Linear(hiddenLayer2Size, outputSize))

criterion = nn.MSECriterion() 

x,columnNames_x = csv2tensor.load("datasetX.txt")
y,columnNames_y = csv2tensor.load("datasetY.txt")

trainDataSize = x:size(1)*0.8

trainX = torch.DoubleTensor(trainDataSize, inputSize)
trainY = torch.DoubleTensor(trainDataSize, outputSize)

for i=1,trainDataSize do
  trainX[i] = x[i]
  trainY[i] = y[i]
end

test={};
function test:size() return x:size(1)*0.2 end

j = 1
for i=trainDataSize+1,x:size(1) do
	test[j] = {x[i], y[i]}
	j = j + 1
end	

x, dl_dx = neuralnet:getParameters()

feval = function(x_new)
	if x ~= x_new then
		x:copy(x_new)
	end

	dl_dx:zero()

	local prediction = neuralnet:forward(trainX)
	local loss_x = criterion:forward(prediction, trainY)
	neuralnet:backward(trainX, criterion:backward(prediction, trainY))

	return loss_x, dl_dx
end


sgd_params = {
   learningRate = 0.1,
   learningRateDecay = 1e-4,
   weightDecay = 0,
   momentum = 0
}

for i = 1, numIterations do
	 _, fs = optim.sgd(feval,x, sgd_params)

	currentError = fs[1] / rho
	print('error for iteration ' .. i  .. ' is ' .. currentError)
	logger:add{['training error'] = currentError}
    logger:style{['training error'] = '-'}
    logger:display(false)
    logger:plot() 
end

neuralnet:evaluate()
err = 0

for iteration = 1, test:size() do
    local sample = test[ math.random(test:size()) ]
    local inputs = sample[1]
    local targets = sample[2]

    local outputs = neuralnet:forward(inputs)
    err = err + criterion:forward(outputs, targets)
end 

err = err / test:size()
print("Testing results: err=" .. err)