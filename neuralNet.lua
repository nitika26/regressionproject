require 'torch'
require 'nn'
require 'CustomStochasticGradient'

csv2tensor = require 'csv2tensor'

neuralnet = nn.Sequential()

inputSize = 18
outputSize = 6

hiddenLayer1Size = 18
hiddenLayer2Size = 18
learningRate = 0.1
numIterations = 50

neuralnet:add(nn.Linear(inputSize, hiddenLayer1Size))
neuralnet:add(nn.ReLU())
neuralnet:add(nn.Dropout(0.1))
neuralnet:add(nn.Linear(hiddenLayer1Size, hiddenLayer2Size))
neuralnet:add(nn.ReLU())
neuralnet:add(nn.Dropout(0.1))
neuralnet:add(nn.Linear(hiddenLayer2Size, outputSize))

criterion = nn.MSECriterion() 

trainer = nn.CustomStochasticGradient(neuralnet, criterion)
trainer.learningRate = learningRate
trainer.maxIteration = numIterations

x,columnNames_x = csv2tensor.load("datasetX.txt")
y,columnNames_y = csv2tensor.load("datasetY.txt")

train={};
function train:size() return x:size(1)*0.8 end 

for i=1,train:size() do
  train[i] = {x[i], y[i]}
end

test={};
function test:size() return x:size(1)*0.2 end

j = 1
for i=train:size()+1,x:size(1) do
	test[j] = {x[i], y[i]}
	j = j + 1
end	

-- Training
trainer:train(train)

neuralnet:evaluate()
err = 0

-- Testing
for iteration = 1, test:size() do
    local sample = test[ math.random(test:size()) ]
    local inputs = sample[1]
    local targets = sample[2]

    local outputs = neuralnet:forward(inputs)
    err = err + criterion:forward(outputs, targets)
end 

err = err / test:size()
print("Testing results: err=" .. err)