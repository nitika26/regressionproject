import numpy as np
import os
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.multioutput import MultiOutputRegressor
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.kernel_ridge import KernelRidge

currentDir = os.path.dirname(os.path.realpath(__file__))

dataX = np.loadtxt(currentDir + "/datasetX.txt", delimiter=',')
dataY = np.loadtxt(currentDir + "/datasetY.txt", delimiter=',')

trainSize = (int) (len(dataX) * 0.8)
max_depth = 30

X_train, X_test, y_train, y_test = train_test_split(dataX, dataY, train_size=trainSize, random_state=4)

regr_multirf = MultiOutputRegressor(RandomForestRegressor(max_depth=max_depth, random_state=0))
regr_multirf.fit(X_train, y_train)

y_multirf = regr_multirf.predict(X_test)
error = mean_squared_error(y_test, y_multirf)
print "MSE on testing data for Random Forest Regressor is : " + str(error)

regr_multigb = MultiOutputRegressor(GradientBoostingRegressor(random_state=0))
regr_multigb.fit(X_train, y_train)
y_multigb = regr_multigb.predict(X_test)

error = mean_squared_error(y_test, y_multigb)
print "MSE on testing data for Gradient Boosting Regressor is : " + str(error)
